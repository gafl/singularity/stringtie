# stringtie Singularity container
### Bionformatics package stringtie<br>
StringTie is a fast and highly efficient assembler of RNA-Seq alignments into potential transcripts. https://ccb.jhu.edu/software/stringtie/<br>
stringtie Version: 2.1.2<br>
[https://github.com/gpertea/stringtie]

Singularity container based on the recipe: Singularity.stringtie_v2.1.2

Package installation using Miniconda3-4.7.12<br>

Image singularity (V>=3.3) is automatically build and deployed (gitlab-ci) and pushed on the registry using the .gitlab-ci.yml <br>

### build:
`sudo singularity build stringtie_v2.1.2.sif Singularity.stringtie_v2.1.2`

### Get image help
`singularity run-help ./stringtie_v2.1.2.sif`

#### Default runscript: STAR
#### Usage:
  `stringtie_v2.1.2.sif --help`<br>
    or:<br>
  `singularity exec stringtie_v2.1.2.sif stringtie --help`<br>


### Get image (singularity version >=3.3) with ORAS:<br>
`singularity pull stringtie_v2.1.2.sif oras://registry.forgemia.inra.fr/gafl/singularity/stringtie/stringtie:latest`


